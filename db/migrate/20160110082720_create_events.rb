class CreateEvents < ActiveRecord::Migration
  def change
    create_table :events do |t|
      t.string :title
      t.string :location
      t.text :details
      t.string :organiser

      t.timestamps null: false
    end
  end
end
